import React from 'react'
import { FaFacebook, FaInstagram, FaTwitter } from 'react-icons/fa'
import {
    FooterContainer,
    FooterWrap,
    SocialMedia,
    SocialMediaWrap,
    SocialLogo,
    SocialIcons,
    SocialIconLink
} from './FooterElements';

const Footer = () => {
    return (
        <FooterContainer>
            <FooterWrap>
                <SocialMedia>
                    <SocialMediaWrap>
                        <SocialLogo to="/">Our Social Media</SocialLogo>
                            <SocialIcons>
                                <SocialIconLink href="/" target="_blank"
                                aria-label="Facebook" rel="noopener
                                noreferrer">
                                    <FaFacebook/>Martabak Aneka Koba
                                </SocialIconLink>
                                <SocialIconLink href="/" target="_blank"
                                aria-label="Instagram" rel="noopener
                                noreferrer">
                                    <FaInstagram/>@Martabak Aneka Koba
                                </SocialIconLink>
                                <SocialIconLink href="/" target="_blank"
                                aria-label="Whatsapp" rel="noopener
                                noreferrer">
                                    <FaTwitter/>
                                </SocialIconLink>
                            </SocialIcons>
                    </SocialMediaWrap>
                </SocialMedia>
            </FooterWrap>
        </FooterContainer>
            
 
    )
}

export default Footer
