import React from 'react'
import { FeatureContainer, FeatureButton } from './FeatureElements';
const Feature = () => {
    return (
      <FeatureContainer>
        <h1>The Taste of Martabak </h1>
        <p>Martabak manis atau yang aslinya bernama Hok Lo Pan awalnya adalah Makanan Khas Bangka Belitung</p>
        <FeatureButton>Order Now</FeatureButton>
      </FeatureContainer>
    );
  };

export default Feature
