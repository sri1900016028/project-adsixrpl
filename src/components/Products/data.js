import product1 from "../../images/food1.jpg";
import product2 from "../../images/food2.jpg";
import product3 from "../../images/food3.jpg";
import product4 from "../../images/food4.jpg";
import product5 from "../../images/food5.jpg";
import drink1 from "../../images/drink1.jpg";
import drink2 from "../../images/drink3.jpg";
import drink3 from "../../images/drink4.jpg";

export const productData = [
    {
      img: product1,
      alt: 'Martabak',
      name: 'Martabak Coklat',
      desc:
        'Martabak coklat silverqueen',
      price: 'Rp15.000',
      button: 'Available'
    },
    {
      img: product2,
      alt: 'Martabak',
      name: 'Martabak Keju',
      desc:
        'Martabak keju cheedar',
      price: 'Rp18.000',
      button: 'Available'
    },
    {
      img: product3,
      alt: 'Martabak',
      name: 'Martabak Kejulat',
      desc:
        'Kombinasi Martabak 2 rasa',
      price: 'RP20.000',
      button: 'Available'
    },
    {
      img: product4,
      alt: 'Martabak',
      name: 'Martabak Telur',
      desc:
        'Martabak Telur Gurih',
      price: 'RP30.000',
      button: 'Available'
    },
    {
      img: product5,
      alt: 'Martabak',
      name: 'Martabak 2 Rasa',
      desc:
        'Kombinasi Martabak 2 rasa',
      price: 'RP25.000',
      button: 'Available'
    }
  ];

  export const productDataTwo = [
    {
      img: drink1,
      alt: 'Kopi',
      name: 'Kopi Boba Gula Aren',
      desc:
        'Perpaduan Rasa Kopi dengan Gula Ren',
      price: 'Rp15.000',
      button: 'Available'
    },
    {
      img: drink2,
      alt: 'Kopi',
      name: 'Kopi Susu Boba',
      desc:
        'Perpaduan Susu Murni dengan Kopi',
      price: 'Rp18.000',
      button: 'Available'
    },
    {
      img: drink3,
      alt: 'Kopi',
      name: 'Kopi Boba Cheese',
      desc:
        'Perpaduan Kopi dengan Keju Boba',
      price: 'RP20.000',
      button: 'Available'
    }
  ];